FROM maven:3.5.2-jdk-8-alpine AS MAVEN_DIR

COPY pom.xml /tmp/

COPY src /tmp/src/

WORKDIR /tmp/

RUN mvn package

FROM openjdk:8-jdk-alpine

COPY --from=MAVEN_DIR tmp/target/moneymanager-api-*.jar moneymanager-api.jar

ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/moneymanager-api.jar"]
