package com.guilherme.moneymanager.api.repository;

import com.guilherme.moneymanager.api.model.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
