package com.guilherme.moneymanager.api.dto;

public class CategoriaDTO {

    private String nome;

    public CategoriaDTO() {

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
