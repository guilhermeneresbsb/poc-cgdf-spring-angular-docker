package com.guilherme.moneymanager.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class MoneymanagerApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoneymanagerApiApplication.class, args);
    }
}
