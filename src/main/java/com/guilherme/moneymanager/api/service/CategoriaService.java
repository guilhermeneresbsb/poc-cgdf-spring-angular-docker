package com.guilherme.moneymanager.api.service;

import com.guilherme.moneymanager.api.dto.CategoriaDTO;
import com.guilherme.moneymanager.api.model.Categoria;
import com.guilherme.moneymanager.api.repository.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.util.List;

@Service
public class CategoriaService {

    @Autowired
    private CategoriaRepository categoriaRepository;

    @Cacheable("categorias")
    public List<Categoria> listar() {
        return categoriaRepository.findAll();
    }

    public ResponseEntity<Categoria> salvar(Categoria categoria, HttpServletResponse response) {
        Categoria categoriaSalva = this.categoriaRepository.save(categoria);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}")
                .buildAndExpand(categoriaSalva.getCodigo()).toUri();

        response.setHeader("Location", uri.toASCIIString());

        return ResponseEntity.created(uri).body(categoriaSalva);
    }
}
