-- CREATE TABLE categoria(
--   codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
--   nome VARCHAR(50) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE categoria(
                        codigo  serial PRIMARY KEY,
                        nome    varchar(50) NOT NULL
);

INSERT INTO categoria(nome) values ('Lazer');
INSERT INTO categoria(nome) values ('Alimentacão');
INSERT INTO categoria(nome) values ('Supermercado');
INSERT INTO categoria(nome) values ('Farmácia');
INSERT INTO categoria(nome) values ('Outros');
